# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' completions 1
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' file-sort name
zstyle ':completion:*' glob 1
zstyle ':completion:*' ignore-parents parent pwd .. directory
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}'
zstyle ':completion:*' max-errors 2
zstyle ':completion:*' menu select=1
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' substitute 1
zstyle :compinstall filename '/home/brad/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configure by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory autocd beep extendedglob
unsetopt nomatch notify
bindkey -v
# End of lines configured by zsh-newuser-install

# backspace and ^h working even after returning from command mode
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char

# Better searching in command mode
bindkey -M vicmd '?' history-incremental-search-backward
bindkey -M vicmd '/' history-incremental-search-forward

# Beginning search with arrow keys
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search
bindkey -M vicmd "k" up-line-or-beginning-search
bindkey -M vicmd "j" down-line-or-beginning-search

# Make vi mode trasitions faster (KEYTIMEOUT is in hundredths of a second)
export KEYTIMEOUT=1

# Use cursor shape to indicate mode
function zle-keymap-select {
	if [[ "$KEYMAP" == "vicmd" ]]; then
		echo -ne '\e[2 q' # solid block
	else
		echo -ne '\e[4 q' # underscore
	fi
}
zle -N zle-keymap-select
echo -ne '\e[4 q' # underscore cursor for new prompt

preexec() {
	echo -ne '\e[4 q' # underscore cursor for new prompt
}

# custom prompt
PROMPT='[%F{green}%m%f:%~] '

git_prompt() {
	GIT_DIR=$(git rev-parse --git-dir 2> /dev/null)
	if [ -z "$GIT_DIR" ]; then
		return 0
	fi
	GIT_HEAD=$(cat "$GIT_DIR/HEAD")
	GIT_BRANCH="${GIT_HEAD##*/}"
	if [ ${#GIT_BRANCH} -eq 40 ]; then
		GIT_BRANCH="NO BRANCH"
	fi
	STATUS=$(git status --porcelain)
	if [ -z "$STATUS" ]; then
		GIT_COLOR="%F{blue}"
	else
		echo -e "$STATUS" | grep -q '^.[A-Z\?]'
		if [ $? -eq 0 ]; then
			GIT_COLOR="%F{red}"
		else
			GIT_COLOR="%F{yellow}"
		fi
	fi
	echo "%B${GIT_COLOR}(${GIT_BRANCH})%f%b"
}

rpromptcmd() { RPROMPT="$(git_prompt)" }
precmd_functions=(rpromptcmd)

alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias ll='ls -alF'
alias la='ls -A'
alias rm='rm -i'
alias rmr='rm -rf'
alias listpath='echo $PATH | tr ":" "\n"'

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

# Add Stack ghc to PATH
export PATH="$(stack path --compiler-bin):$PATH"

# Add .local/bin to PATH
export PATH="${HOME}/.local/bin:$PATH"
